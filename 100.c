#include <stdio.h>


int main(int argc, char *argv[])
{
	int x, y, i, count, max = 0;
	int low, high, tmp = 0;
	while(scanf("%d %d", &x, &y) != EOF)
	{
		tmp = 0;
		if (x>y) {
			high = x;
			low = y;
		} else {
			low = x;
			high = y;
		}
		max = 0;
		for(i = low; i >= low && i <= high; i++){
			tmp = i;
			count = 1;
			while(1){
				if(tmp == 1){
					if (count > max){
						max = count;
					}
					break;
				}
				if(tmp %2 == 1){
					tmp = 3*tmp + 1;
				} else {
					tmp = tmp/2;
				}
				count++;
			}
		}
		printf("%d %d %d\n", x, y, max);
	}
	return 0;
}